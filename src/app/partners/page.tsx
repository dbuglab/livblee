"use client"
import React ,{useState, useEffect, useRef }from 'react'
import Header from "../components/Header";
import SliderComponent from "../components/SliderComponent";
import { useForm } from 'react-hook-form';
import { motion , useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
import { Canvas } from 'react-three-fiber';
import Footer from "../components/Footer";
import { useSpring } from 'react-spring';
import ThreeDImage from '../components/ThreeImage';
import Dashboard from '../components/Dashboard';
import { Exo_2 } from 'next/font/google';

const page = () => {
  const [scrollProgress, setScrollProgress] = useState(0);
  useEffect(() => {
    const handleScroll = () => {
      const progress = window.scrollY / window.innerHeight;
      setScrollProgress(progress);
    };

    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);
 
  const springProps = useSpring({
    from: { opacity: 0 },
    to: { opacity: 1 },
  });
  const bannerVariants = {
    hide: {
      opacity: 0,
      y: 50,
    },
    show: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
      },
    },
  };
  const imageRef = useRef(null);
  const controls = useAnimation();
  useEffect(() => {
    // Animate the trapezoid to straight on component mount
    controls.start({ rotateX: 0 });

    // Cleanup animation on component unmount
    return () => {
      controls.stop();
    };
  }, [controls]);
  const [ref, inView] = useInView({
    triggerOnce: true, 
    delay: 1000,
  });
  const [ref1, inView1] = useInView({
    triggerOnce: true, 
    delay: 1000,
  });
  const [ref2, inView2] = useInView({
    triggerOnce: true, 
    delay: 1000,
  });
  const [ref3, inView3] = useInView({
    triggerOnce: true, 
    delay: 1000,
  });
  const {
    handleSubmit,
    register,
    control,
    reset,
    getValues,
    formState: { errors },
  } =  useForm();

  const onSubmit = (data) => {
    console.log(data);
    // Handle form submission logic here

    const formValues = getValues();
    console.log('Name:', formValues.name);
    console.log('Company:', formValues.company);
    console.log('Phone:', formValues.phone);
    console.log('Email:', formValues.email);
    console.log('Message:', formValues.message);

    reset();
  };
  const banner: Variants = {
    hide: {
      opacity: 0,
      y: 50,
      z: 50, 
     // Change to a positive value for "down to up"
    },
    show: {
      opacity: 1,
      y: 0,
     
    
      transition: {
        duration: 2,
      },
    },
  };
  const introPictureVariants: Variants = {
    hide: {
        opacity: 0,
        x: 0,
    },
    show: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 0.6,
        ease: 'easeInOut',
      },
    },
};

const propertyIconImg = {
  hide: {
    opacity: 0,
    y: 50,
  },
  show:{
    opacity: 1,
    y: 0,
    transition: {
      duration: 1,
      ease: 'easeInOut',
       // Adjust the delay here
    },
  },
};
const propertyIcon = {
  hide: {
    opacity: 0,
    y: 50,
  },
  show: (index) => ({
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.6,
      ease: 'easeInOut',
      delay: index * 0.5, // Adjust the delay here
    },
  }),
};
const listItemVariants = {
  hide: {
    opacity: 0,
    y: 50,
  },
  show: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 1,
      ease: 'easeInOut',
    },
  },
};

  return (
    <>
    <Header currentPage="/otherPage"/>
    <motion.div
      initial="hide"
      animate="show"
      exit="hide"
      transition = {{ delay: 2, duration: 0.7 , ease: 'easeInOut' } }
      variants={bannerVariants}
    >
      
    <div className="property-bnr">
    <div className="container">
      <div className="prop-bnr-title-btn">
        <h1>
          Embedded flexible rent
          <br />
          <span>built for software.</span>
        </h1>
        <div className="property-btn">
          <a href="#">Book a Demo</a>
          <a href="#">Get the API keys</a>
        </div>
      </div>
      <div className="prop-bnr-img-container">
            <motion.div
          className="prop-bnr-img"
          initial={{ scale: 0,skewX: 0 }} 
          animate={{ scale: 1 ,skewY: 0 }} 
          transition={{ duration: 0.7 , ease: 'easeInOut' }}
        >
         <Dashboard/>
          
        </motion.div>
        </div>
    </div>
  </div>
  </motion.div>
  <div className="logo-sec mt0">
    <div className="logo-title">
      <h2>Trusted by</h2>
    </div>
    <SliderComponent/>
  </div>
  <motion.div
      
      initial="hide"
      whileInView="show"
      exit="hide"
      animate={inView ? "show" : "hide"}
     // animate={{ opacity: 0, y: 50 }}
      transition = {{duration: 2, ease: 'easeInOut' } }
      variants={introPictureVariants}
    >
  <div className="property-icon">
    <div className="container">
      <div className="property-icon-title">
        <h2>
          Livble <span>provides</span>
        </h2>
      </div>
      <motion.div 
     
      className="prop-icn-sec" 
      transition = {{delay: 2, duration: 2, ease: 'easeInOut' } }
      variants={propertyIconImg}
      animate={inView ? "show" : "hide"}
      >
          
        <div className="pro-icn-div">
        <motion.img
                ref={ref}
                src="images/Frame%207419.png"
                initial="hide"
                whileInView="show"
                exit="hide"
                animate={{ opacity: 0, y: 50 }}
                variants={propertyIconImg}
                transition={{  delay: 0.5, ease: 'easeInOut' }} // Adjust the duration here
              />
          <h4>Risk-free additional<br />revenue stream</h4>
          <p>Access a new revenue channel without bearing any credit risk</p>
        </div>
        <div className="pro-icn-div">
        <motion.img
        
                src="images/Frame%207420.png"
                initial="hide"
                whileInView="show"
                exit="hide"
                animate={{ opacity: 0, y: 50 }}
                variants={propertyIconImg}
                transition={{  delay: 0.9, ease: 'easeInOut' }} // Adjust the duration here
              />
         
          <h4>Quick Launch!</h4>
          <p>
            Launch it in days with our industry leading API documentation.
            Livble does the heavy lifting!
          </p>
        </div>
        <div className="pro-icn-div">
        <motion.img
        
                src="images/Frame%207421.png"
                initial="hide"
                whileInView="show"
                exit="hide"
                animate={{ opacity: 0, y: 50 }}
                variants={propertyIconImg}
                transition={{  delay: 0.9, ease: 'easeInOut' }} // Adjust the duration here
              />
          
          <h4>End-to-end lending infrastructure</h4>
          <p>
          From underwriting &amp; funding to collections, we run it all!
          </p>
        </div>
        </motion.div>
    </div>
  </div>
  </motion.div>
  <motion.div
      initial="hide"
      whileInView="show"
      exit="hide"
      // animate={{ opacity: 0, y: 50 }}
      animate={inView ? "show" : "hide"}
      transition = {{ delay: 2, duration: 2, ease: 'easeInOut' } }
      variants={introPictureVariants}
    >
  <div className="property-icon">
    <div className="container">
      <div className="property-icon-title">
        <h2>
          Software <span>benefits</span>
        </h2>
      </div>
      <motion.div 
      className="prop-icn-sec"
       variants={propertyIconImg}
       transition = {{ delay: 2, duration: 2, ease: 'easeInOut' } }
       animate={inView1 ? "show" : "hide"}
       >
        <div className="pro-icn-div">
          <motion.img
           ref={ref1}
                src="images/Frame%207424.png"
                initial="hide"
                whileInView="show"
                exit="hide"
                animate={{ opacity: 0, y: 50 }}
                variants={propertyIconImg}
                transition={{  delay: 0.5, ease: 'easeInOut' }} // Adjust the duration here
              />
          <h4>Improve your offering!</h4>
          <p>
            Provide your tenants with the flexible payment solution they are
            asking for!
          </p>
        </div>
        <div className="pro-icn-div">
          <motion.img
                src="images/Frame%207422.png"
                initial="hide"
                whileInView="show"
                exit="hide"
                animate={{ opacity: 0, y: 50 }}
                variants={propertyIconImg}
                transition={{  delay: 0.9, ease: 'easeInOut' }} // Adjust the duration here
              />
          <h4>Competitive edge</h4>
          <p>
            Win business by guaranteeing more rent on time, and improving NOI
            for your clients
          </p>
        </div>
        <div className="pro-icn-div">
          
          <motion.img
          
                src="images/Frame%207423.png"
                initial="hide"
                whileInView="show"
                exit="hide"
                animate={{ opacity: 0, y: 50 }}
                variants={propertyIconImg}
                transition={{  delay: 1.5, ease: 'easeInOut' }} // Adjust the duration here
              />
          <h4>Activate offline tenants</h4>
          <p>
          Migrate your cash &amp; check tenants to online payments with Livble
          </p>
        </div>
      </motion.div>
    </div>
  </div>
  </motion.div>
  <motion.div
      initial="hide"
      whileInView="show"
      exit="hide"
     // animate={{ opacity: 0, y: 50 }}
      animate={inView ? "show" : "hide"}
      transition = {{delay:2,duration: 0.6, ease: 'easeInOut' } }
      variants={introPictureVariants}
    >
  <div className="poperty-detil-reviw">
    <div className="container">
      <div className="manage-review-main">
        <div className="manage-reviw-left">
          <h3>
            Why <span>Property Managers</span> love Livble
          </h3>
          <motion.ul variants={listItemVariants}>
          <motion.li>
                  <div className="manage-icn">
                    <img src="images/V.png" />
                  </div>
                  <div className="manage-cnt">
                    <h6>Rent Certainty</h6>
                    <p>Get paid on time in full</p>
                  </div>
                </motion.li>
                <motion.li>
                  <div className="manage-icn">
                    <img src="images/V.png" />
                  </div>
                  <div className="manage-cnt">
                    <h6>Reduce Costs </h6>
                    <p>Reduce operational costs associated with rent collections</p>
                  </div>
                </motion.li>
                <motion.li>
                  <div className="manage-icn">
                    <img src="images/V.png" />
                  </div>
                  <div className="manage-cnt">
                    <h6>Happier Residents </h6>
                    <p>
                      Improve relationship with your residents by providing them
                      flexibility
                    </p>
                  </div>
                </motion.li>
                </motion.ul>
        </div>
        <div className="manage-reviw-right">
         
          <motion.img
                src="images/Rectangle%20146.png"
                initial="hide"
                whileInView="show"
                exit="hide"
                animate={{ opacity: 0, y: 50 }}
                variants={propertyIconImg}
                transition={{  delay: 1.5, ease: 'easeInOut' }} // Adjust the duration here
              />
          <div className="reviw">
            <img src="images/new.png" />
            <h6>
               Since our software added Livble, we've streamlined rent collection and also
                retained more tenants. It's a win-win!
            </h6>
            <p>Sarah Williams, Property manager, NY</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  </motion.div>
  <motion.div
     initial="hide"
     whileInView="show"
     exit="hide"
    // animate={{ opacity: 0, y: 50 }}
     animate={inView ? "show" : "hide"}
     transition = {{ delay: 2,duration: 0.6, ease: 'easeInOut' } }
     variants={introPictureVariants}
    >
  <div className="poperty-detil-reviw prop-btm">
    <div className="container">
      <div className="manage-review-main">
        <div className="manage-reviw-left">
          <h3>
            Why <span>Tenants</span> love Livble
          </h3>
          <motion.ul variants={listItemVariants}>
          <motion.li>
              <div className="manage-icn">
                <img src="images/V.png" />
              </div>
              <div className="manage-cnt">
                <h6>Flexible rent payments</h6>
                <p>
                  Provide your tenants with the payment flexibility they need
                </p>
              </div>
              </motion.li>
              <motion.li>
              <div className="manage-icn">
                <img src="images/V.png" />
              </div>
              <div className="manage-cnt">
                <h6>Boost tenant credit scores</h6>
                <p>Enhance tenant credit scores with on-time rent payments</p>
              </div>
              </motion.li>
              <motion.li>
              <div className="manage-icn">
                <img src="images/V.png" />
              </div>
              <div className="manage-cnt">
                <h6>Empower financial management</h6>
                <p>
                  Support your tenants in getting their finances back on track
                </p>
              </div>
              </motion.li>
            </motion.ul>
        </div>
        <div className="manage-reviw-right">
          <motion.img
                src="images/Rectangle%20195.png"
                initial="hide"
                whileInView="show"
                exit="hide"
                animate={{ opacity: 0, y: 50 }}
                variants={propertyIconImg}
                transition={{  delay: 1.5, ease: 'easeInOut' }} // Adjust the duration here
              />
          <div className="reviw">
            <img className='left-img' src="images/new.png" />
            <h6>
              Livble made rent splitting easy, giving me financial flexibility.
              I'm grateful for the peace of mind it brings
            </h6>
            <p>Renee Waters, Tenant, WA</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  </motion.div>
  
  <div className="ftr-upr-main">
  <div className="container">
    <div className="ftr-upr">
  <div className="ftr-title">
    <h2>
      Get Started <span>Today!</span>
    </h2>
  </div>
  <div className="ftr-frm">
  {/* {errors.name && <span style={{ color: 'white' }}>{errors.name.message}</span>} */}
  <form >
  {/* //  onSubmit={handleSubmit(onSubmit)} */}
  
  <input type="text" placeholder="Name" 
  // {...register('name', { required: 'Full name is required' })} 
  />
        
        <input type="text"  placeholder="Company" 
        // {...register('company')} 
        />
        <input
          type="tel"  placeholder="Phone"
          // {...register('phone', {
          //   pattern: {
          //     value: /^\d{10}$/,
          //     message: 'Please enter a valid 10-digit phone number',
          //   },
          // })}
        />
        {/* {errors.phone && <span>{errors.phone.message}</span>} */}
        <input
          type="email"  placeholder="Email"
          // {...register('email', {
          //   required: 'Email address is required',
          //   pattern: {
          //     value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
          //     message: 'Please enter a valid email address',
          //   },
          // })}
        />
        {/* {errors.email && <span style={{ color: 'white' }}>{errors.email.message}</span>} */}
      <textarea placeholder="Message" defaultValue={""}
      //  {...register('message')} 
       />
      <button type="button">Send</button>
    </form>
  </div>
</div>
</div>
</div>

<Footer/>
   
</>

  )
}

export default page
