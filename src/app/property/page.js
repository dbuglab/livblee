"use client"
import React, { useRef } from "react";

export default function Home() {
  const ref = useRef(null);
  React.useEffect(() => {
    import("@lottiefiles/lottie-player");
  });
  return (
    <div >
      <main >
        <lottie-player
          id="firstLottie"
          ref={ref}
          autoplay
          controls
          speed="0.8"
          loop
          mode="normal"
          src="lottie/Widget_1_desktop.json"
          style={{ width: "300px", height: "300px" }}
        ></lottie-player>
      </main>
    </div>
  );
}