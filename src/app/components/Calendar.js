// components/Calendar.js

import React, { useState } from "react";
import styles from "./Calendar.module.css";

const Calendar = ({ isToggled, onSelectDate, onRadioClick2 }) => {
  const [selectedDate, setSelectedDate] = useState(new Date());

  const daysInMonth = (year, month) => new Date(year, month + 1, 0).getDate();

  const getMonthData = (year, month) => {
    const firstDay = new Date(year, month, 1);
    const daysInMonthCount = daysInMonth(year, month);
    const startOffset = (firstDay.getDay() + 6) % 7;
    //console.log(daysInMonthCount)
    return Array.from({ length: daysInMonthCount }, (_, i) => i + 1).map(
      (day) => ({
        day,
        date: new Date(year, month, day),
      })
    );
  };
  const handleDateClick = (date) => {
    setSelectedDate(date);
    onSelectDate(date);
    onRadioClick2(date);

    // Call the callback function with the selected date
  };

  const handlePrevMonth = () => {
    setSelectedDate(
      new Date(selectedDate.getFullYear(), selectedDate.getMonth() - 1, 1)
    );
  };

  const handleNextMonth = () => {
    setSelectedDate(
      new Date(selectedDate.getFullYear(), selectedDate.getMonth() + 1, 1)
    );
  };

  const monthData = getMonthData(
    selectedDate.getFullYear(),
    selectedDate.getMonth()
  );
  //   const str = monthData[0].date.toString();

  // const first = str.split(' ')[0]
  // console.log(first);
  // console.log(monthData[0].date)
  const isSameDay = (date1, date2) => {
    return (
      date1.getDate() === date2.getDate() &&
      date1.getMonth() === date2.getMonth() &&
      date1.getFullYear() === date2.getFullYear()
    );
  };
  const getWeekdayLabels = () => {
    const weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    const firstDayOfMonth = new Date(
      selectedDate.getFullYear(),
      selectedDate.getMonth(),
      1
    );
    const startOffset = firstDayOfMonth.getDay();
    return [...weekdays.slice(startOffset), ...weekdays.slice(0, startOffset)];
  };
  const firstDayOfMonth = new Date(
    selectedDate.getFullYear(),
    selectedDate.getMonth(),
    1
  );
  const startOffset = (firstDayOfMonth.getDay() + 7) % 7;
  return (
    <div
      className={`${styles.calendar} ${
        isToggled ? styles.visible : styles.hidden
      }`}
    >
      <div className={styles.header}>
        <button className={styles.button}
        //  onClick={handlePrevMonth}
         >
          <img src="images/Arrow.png" alt="Previous Month" />
        </button>
        <h2>
          {new Date(selectedDate).toLocaleDateString("en-US", {
            month: "long",
            year: "numeric",
          })}
        </h2>
        <button className={styles.button} 
        // onClick={handleNextMonth}
        >
          <img src="images/Arrow (1).png" alt="Previous Month" />
        </button>
      </div>
      <div className={styles.grid}>
        {/* {getWeekdayLabels().map((day, index) => (
    <div key={index} className={styles.dayLabel}>
      {day}
    </div>
  ))} */}
        {[...Array(startOffset)].map((_, index) => (
          <div key={`empty-before-${index}`} className={styles.emptySlot}>
            {daysInMonth(
              selectedDate.getFullYear(),
              selectedDate.getMonth() - 1
            ) -
              startOffset +
              index +
              1}
          </div>
        ))}
        {monthData.map((item) => (
          <div
            key={item.date.toISOString()}
            className={`${styles.day} ${
              isSameDay(item.date, selectedDate) ? styles.selected : ""
            }
      ${
        isSameDay(
          item.date,
          new Date(selectedDate.getTime() - 7 * 24 * 60 * 60 * 1000)
        )
          ? styles.beforeSeven
          : ""
      }
      ${
        isSameDay(
          item.date,
          new Date(selectedDate.getTime() + 7 * 24 * 60 * 60 * 1000)
        )
          ? styles.afterSeven
          : ""
      }
      `}
            // onClick={() => handleDateClick(item.date)}
          >
            {item.day}
          </div>
        ))}
        {[...Array(6 * 7 - startOffset - monthData.length)].map((_, index) => (
          <div key={`empty-after-${index}`} className={styles.emptySlot}>
            {index + 1}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Calendar;
