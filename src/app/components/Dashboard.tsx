import React, { useState, useEffect, useRef, useLayoutEffect } from "react";
import Slider1 from "./dash/Slider1";
import Slider2 from "./dash/Slider2";
import Slider3 from "./dash/Slider3";
import {
  motion,
  useAnimation,
  useViewportScroll,
  useTransform,
  useScroll,
  useTime,
} from "framer-motion";
import { useInView } from "react-intersection-observer";
import { Canvas, useThree, useFrame } from "@react-three/fiber";
import { degreesToRadians, progress, mix } from "popmotion";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

const Dashboard = () => {
  const [isMobile, setIsMobile] = useState(false);
  const reff = useRef(null);
  React.useEffect(() => {
    import("@lottiefiles/lottie-player");
  });
  const handleScroll = () => {
    // Check if the user has scrolled down
    if (window.scrollY > 0) {
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }
  };
  const controls = useAnimation();
  const [ref, inView] = useInView({
    triggerOnce: true, // Only trigger once when it comes into view
  });
  const [currentStep, setCurrentStep] = useState(1);
  const handleAnimationComplete = () => {
    console.log("Before:", currentStep);
    setCurrentStep((currentStep) => (currentStep % 3) + 1);
    console.log("After:", currentStep);
  };
  const renderStep = () => {
    switch (currentStep) {
      case 1:
        return <Slider1 />;
      case 2:
        return <Slider2 />;
      case 3:
        return <Slider3 />;
      default:
        return null;
    }
  };
  useEffect(() => {
    // Add scroll event listener
    window.addEventListener("scroll", handleScroll);

    // Clean up the event listener on component unmount
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  useEffect(() => {
    if (inView) {
      controls.start("show");
    } else {
      controls.start("hide");
    }
    const animationDuration = 0.8; // Adjust the duration of your animation
    const intervalDuration = 5000;
    const intervalId = setInterval(() => {
      setTimeout(() => {
        handleAnimationComplete();
      }, (intervalDuration - animationDuration * 1000) / 2); // Adjust the timeout to match the duration of your animation
    }, 5000);

    // Cleanup interval on component unmount
    return () => clearInterval(intervalId);
  }, [controls, inView]);
  const [ref3, inView3] = useInView({
    triggerOnce: true,
    threshold: 1,
  });
  useEffect(() => {
    if (ref3.current && inView3) {
      // Reset Lottie animation when becoming visible
      Lottie.play(ref3.current);
    }
  }, [inView3]);
  const [inViewA, setInViewA] = useState(false);
  useEffect(() => {
    // Pause for 3 seconds before starting the animation
    const timeoutId = setTimeout(() => {
      setInViewA(true);
    }, 2000);

    return () => clearTimeout(timeoutId); // Clear the timeout if the component unmounts

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
 
  
  return (
    <>
      <motion.div className="dashborad">
        <motion.img src="images/Frame 7333.png" className="dsk-top" />

        <lottie-player
          id="firstLottie"
          ref={inViewA ? ref3 : null}
          autoplay
          key={inView3}
          loop
          speed="0.8"
          mode="normal"
          src='lottie/Partners_Widget.json' 
          style={{ width: "280px", height: "280px",
          visibility: inView3 ? "visible" : "hidden",       
        }}
        ></lottie-player>

<div className="dashborad-mob">
  <picture>
    <source srcSet="images/Frame 7333.svg" type="image/svg+xml" />
    <img src="images/Frame 7333.png" alt="Dashboard Image" />
  </picture>
</div>
      </motion.div>

     
    </>
  );
};

export default Dashboard;

