import React from "react";
import { motion } from "framer-motion";
const Slider2 = () => {
 
  const propertyIconImg = {
    hide: {
      opacity: 0,
      y: 50,
    },
    show:{
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        ease: 'easeInOut',
         // Adjust the delay here
      },
    },
  };
  return (
    <>
      <div className="frame-sec">
       
        <motion.div
        className="frame-up"
        initial="hide"
        whileInView="show"
        exit="hide"
        animate={{ opacity: 0, y: 50 }}
        variants={propertyIconImg}
        transition={{  delay: 0.5, ease: 'easeInOut' }}
      >
          <ul>
            <li>
              {" "}
              <img src="images/Frame%207264.png" />
            </li>
            <li>
              <a >
                <img src="images/Subtract.png" />
                <h6>Installments</h6>
              </a>
            </li>
            <li>
              <a >
                <img src="images/calendar.png" />
                <h6>Dates</h6>
              </a>
            </li>
            <li>
              <a >
                <img src="images/hand-thumb-up.png" />
                <h6>Done</h6>
              </a>
            </li>
          </ul>
       
        <h5>Installments number</h5>
        <p>Select your preferred option</p>
        </motion.div>
       
        <motion.div
        className="frame-num"
        initial="hide"
        whileInView="show"
        exit="hide"
animate={{ opacity: 0, y: 50 }}
        variants={propertyIconImg}
        transition={{  delay: 0.5, ease: 'easeInOut' }}
      >
          <ul>
            <li>
              2 <p>of $750</p>
            </li>
            <li>
              3 <p>of $500</p>
            </li>
            <li style={{ backgroundColor: '#09987F', color: '#fff'}}>
              4 <p style={{ color: '#FFFFFFCC'}}>of $375</p>
            </li>
          </ul>
        </motion.div>
      </div>
    </>
  );
};

export default Slider2;
