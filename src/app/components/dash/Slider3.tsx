import React from "react";
import { motion } from "framer-motion";
const Slider3 = () => {
 
  const propertyIconImg = {
  hide: {
    opacity: 0,
    y: 50,
  },
  show:{
    opacity: 1,
    y: 0,
    transition: {
      duration: 1,
      ease: 'easeInOut',
       // Adjust the delay here
    },
  },
};
  return (
    <>
      <div className="frame-sec">
      <motion.div
        className="frame-up"
        initial="hide"
                whileInView="show"
                exit="hide"
        animate={{ opacity: 0, y: 50 }}
        variants={propertyIconImg}
        transition={{  delay: 0.5, ease: 'easeInOut' }}
      >
          <ul>
            <li>
              {" "}
              <img src="images/Frame%207264.png" />
            </li>
            <li>
              <a >
                <img src="images/Subtract.png" />
                <h6>Installments</h6>
              </a>
            </li>
            <li>
              <a >
                <img src="images/calendar.png" />
                <h6>Dates</h6>
              </a>
            </li>
            <li>
              <a>
                <img src="images/hand-thumb-up.png" />
                <h6>Done</h6>
              </a>
            </li>
          </ul>
          <h5>Installments number</h5>
        <p>Select your preferred option</p>
        </motion.div>
        
      
        <motion.div
       className="bar1"
        initial="hide"
        whileInView="show"
        exit="hide"
        animate={{ opacity: 0, y: 50 }}
        variants={propertyIconImg}
        transition={{  delay: 0.5, ease: 'easeInOut' }}
      >
          <ul>
            <li className="active">
              <span>1</span>
              <h5>Today</h5>
            </li>
            <li>
              <span>2</span>
              <h5>10/08</h5>
            </li>
            <li>
              <span>3</span>
              <h5>10/15</h5>
            </li>
            <li>
              <span>4</span>
              <h5>10/22</h5>
            </li>
          </ul>
        </motion.div>
      </div>
    </>
  );
};

export default Slider3;
