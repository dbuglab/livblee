import React from "react";
import { motion } from 'framer-motion';
const Slider1 = () => {
    const leftContainerVariants = {
        hidden: { x: '-100%', opacity: 0 },
        visible: { x: 0, opacity: 1 },
        exit: { x: '100%', opacity: 0 },
      };
    
      const rightContainerVariants = {
        hidden: { x: '100%', opacity: 0 },
        visible: { x: 0, opacity: 1 },
        exit: { x: '-100%', opacity: 0 },
      };
  return (
    <>
      <div className="bln-left">
      <motion.div
        className="bln-cnt"
        initial="hidden"
        animate="visible"
        exit="exit"
        variants={leftContainerVariants}
        transition={{ duration: 1 }}
      >
          <h4>Split your rent in installments</h4>
          <p>
            Livble helps you spread the cost of your rent and avoid late fees
          </p>
          <a >
            Add <img src="images/Group%209.png" />
          </a>
          </motion.div>
          <motion.div
        className="bln-img"
        initial="hidden"
        animate="visible"
        exit="exit"
        variants={rightContainerVariants}
        transition={{ duration: 1 }}
      >
          <img src="images/Group%206803.png" className="img1" />
          <img src="images/Vector.png" className="img2" />
          <img src="images/Vector-2.png" className="img3" />
          <img src="images/Vector-2.png" className="img4" />
          <img src="images/Vector12.png" className="img5" />
          </motion.div>
      </div>
    </>
  );
};

export default Slider1;
