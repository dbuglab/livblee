import React, { useState, useEffect ,useRef  } from "react";
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";

const Step1 = ({ onRadioClick }) => {
  const [applyStyles, setApplyStyles] = useState(false);
  const controls = useAnimation();
  const reff = useRef(null);
  React.useEffect(() => {
    import("@lottiefiles/lottie-player");
  });
  
  // const [ref, inView] = useInView({
  //   triggerOnce: true, // Only trigger once when it comes into view
  // });
  const introPictureVariants = {
    hide: {
      opacity: 0,
      scale: 0.5, // Change to a positive value for "down to up"
    },
    show: {
      opacity: 1,
      scale: 1, // Final scale when shown
      transition: {
      duration: 0.5,
      },
    },
  };
  // useEffect(() => {
  //   const timeoutId = setTimeout(() => {
  //     setApplyStyles(true);
  //   }, 1500); // Set the delay time in milliseconds

  //   return () => clearTimeout(timeoutId); // Cleanup timeout on component unmount
  // }, []);
  // useEffect(() => {
  //   if (applyStyles) {
  //     controls.start("show");
  //   } else {
  //     controls.start("hide");
  //   }
  // }, [applyStyles, controls]);
  // useEffect(() => {
  //   if (inView) {
  //     controls.start("show");
  //   } else {
  //     controls.start("hide");
  //   }
  // }, [controls, inView]);
  // const labelVariants = {
  //   hide: { opacity: 0, scale: 0 },
  //   show: { opacity: 1, scale: 1 },
  // };
  // const bubbleVariants = {
  //   hidden: {
  //     scale: 0,
  //     opacity: 0,
  //   },
  //   visible: {
  //     scale: 1,
  //     opacity: 1,
  //     transition: {
  //       type: "spring",
  //       stiffness: 500,
  //       damping: 30,
  //     },
  //   },
  // };
  return (
    <motion.div
      className="wrf-right"
      style={{ paddingTop: '10px'}}
      initial="hide"
      animate="show"
      variants={introPictureVariants}
    >
       <lottie-player
          id="firstLottie"
          ref={reff}   
          autoplay
          background="transparent"
         // controls="true"
          speed="0.7"
          mode="normal"
          src="lottie/Widget_1_desktop.json"
          style={{ width: "400px",
                   height: "480px", 
                   border: "1px solid #EAEAED",
                   borderRadius: "20px",
                   boxShadow: "0px 23.852px 83.483px -19.082px rgba(0, 0, 0, 0.12)",                   
                }}
        ></lottie-player>
    </motion.div>
  );
};

export default Step1;
