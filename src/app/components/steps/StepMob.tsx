
import React, { useState, useEffect,useRef } from "react";
import Calendar from "../Calendar";
import LottiePlayer from '../LottiePlayer';
import { useInView } from "react-intersection-observer";

const StepMob = ({onRadioClick2}) => {
  const reff = useRef(null);
  const reff2 = useRef(null);
  const reff3 = useRef(null);
  const [ref1, inView1] = useInView({
    triggerOnce: true,
    threshold: 0.6,
  });

  const [ref2, inView2] = useInView({
    triggerOnce: true,
    threshold: 0.6,
   
  });
  const [ref3, inView3] = useInView({
    triggerOnce: true,
    threshold: 0.6,
  });
  useEffect(() => {
    if (ref1.current && inView1) {
      // Reset Lottie animation when becoming visible
      Lottie.play(ref1.current);
    }
  }, [inView1]);
  useEffect(() => {
    if (ref2.current && inView2) {
      // Reset Lottie animation when becoming visible
      Lottie.play(ref2.current);
    }
  }, [inView2]);
  useEffect(() => {
    if (ref3.current && inView3) {
      // Reset Lottie animation when becoming visible
      Lottie.play(ref3.current);
    }
  }, [inView3]);

  
  React.useEffect(() => {
    import("@lottiefiles/lottie-player");
  });
    const [selectedStep, setSelectedStep] = useState(1);
    const [selectedDate, setSelectedDate] = useState(new Date());
    const [isToggled, setToggled] = useState(true);
    const [selectedItem, setSelectedItem] = useState(2);
    const handleDateSelect = (date) => {
        setSelectedDate(date);
        setToggled(!isToggled);
      };
    const isToday = (date, item) => {
        const today = new Date();
        const targetDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 7 * (item - 1));
        return (
          today.getFullYear() === targetDate.getFullYear() &&
          today.getMonth() === targetDate.getMonth() &&
          today.getDate() === targetDate.getDate()
        );
      };
        const [isPaused, setIsPaused] = useState(false);
        const [isPaused2, setIsPaused2] = useState(false);
        const [isPaused3, setIsPaused3] = useState(false);
      const toggleAnimation = () => {
        const lottiePlayer = reff.current;
        if (lottiePlayer) {
          if (isPaused) {
            lottiePlayer.play();
          } else {
            lottiePlayer.pause();
          }
          setIsPaused(!isPaused);
        }
      };

      const toggleAnimation2 = () => {
        const lottiePlayer = reff2.current;
        if (lottiePlayer) {
          if (isPaused2) {
            lottiePlayer.play();
          } else {
            lottiePlayer.pause();
          }
          setIsPaused2(!isPaused2);
        }
      };
      const toggleAnimation3 = () => {
        const lottiePlayer = reff3.current;
        if (lottiePlayer) {
          if (isPaused3) {
            lottiePlayer.play();
          } else {
            lottiePlayer.pause();
          }
          setIsPaused3(!isPaused3);
        }
      };
     
  return (
    <>
        <div>
      <div className="how-it-work">
        <div className="container">
          <div className="how-work-title">
            <h2>
              How it <span>works</span>
            </h2>
          </div>
          <div className="wek-main">
            <div className="wrk-left">
              <ul>
                <li
                  className=""
                 
                >
                  <h6>Step 1</h6>
                  <h4>Select Livble as your payment method</h4>
                  <p>Available in your tenant portal</p>
                </li>
                <div
      className="wrf-right"
     
    >
       <lottie-player
                      id="firstLottie1"
                      ref={ref1}
                      key={inView1}
                      autoplay={inView1}
                      background="transparent"
                      speed="1"
                     // loop
                      mode="normal"
                      src="lottie/Widget_1_mobile.json"
                      style={{
                        width: "353px",
                        height: "475px",
                        borderRadius: "20px",
                        boxShadow: " 0px 23.852px 83.483px -19.082px rgba(0, 0, 0, 0.12)",
                        visibility: inView1 ? "visible" : "hidden", 
                      }}
                      onClick={toggleAnimation}
                    ></lottie-player>
        
    </div>
                <li 
                  className="" >
                  <h6  >Step 2</h6>
                  <h4>Choose the plan that best suits you</h4>
                  <p>Select the preferred dates</p>
                </li>
                <div
      className="wrf-right"
     
    >
      <lottie-player
          id="firstLottie2"
          ref={ref2}
          key={inView2}
          autoplay={inView2}
          background="transparent"
          speed="1"
         // loop
          mode="normal"
          src="lottie/Widget_2_mobile.json"
          style={{ width: "353px",
                   height: "475px",
                   borderRadius: "20px",
                   boxShadow: " 0px 23.852px 83.483px -19.082px rgba(0, 0, 0, 0.12)", 
                   visibility: inView2 ? "visible" : "hidden",
                }}
                onClick={toggleAnimation2}
        ></lottie-player>
    </div>
                <li 
                  className=""   
                
                >
                  <h6  >Step 3</h6>
                  <h4>Split your rent - we take care of the rest!</h4>
                  <p>
                  You're all set for the month!
                  </p>
                </li>
                <div
      className="wrf-right"
     
    >
    <lottie-player
          id="firstLottie3"
          ref={ref3}
          autoplay={inView3}
          key={inView3}
          background="transparent"
          speed="1"
        //  loop
          mode="normal"
          src="lottie/Widget_3_mobile.json"
          style={{ width: "353px",
                   height: "475px",
                   borderRadius: "20px",
                   boxShadow: " 0px 23.852px 83.483px -19.082px rgba(0, 0, 0, 0.12)", 
                   visibility: inView3 ? "visible" : "hidden",
                }}
                onClick={toggleAnimation3}
        ></lottie-player>
    </div>
              </ul>
            </div>
            {/* {renderStep()} */}
          </div>
        </div>
      </div>
    </div>
    </>
  )
}

export default StepMob

