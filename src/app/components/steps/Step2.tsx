import React, { useState, useEffect ,useRef} from "react";
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
import Calendar from "../Calendar";
const Step2 = ({ onRadioClick2 }) => {
  const controls = useAnimation();
  const reff = useRef(null);
  React.useEffect(() => {
    import("@lottiefiles/lottie-player");
  });
  const [ref, inView] = useInView({
    triggerOnce: true, // Only trigger once when it comes into view
  });
  const introPictureVariants = {
    hide: {
      opacity: 0,
      scale: 0.5, // Change to a positive value for "down to up"
    },
    show: {
      opacity: 1,
      scale: 1, // Final scale when shown
      transition: {
        duration: 0.5,
      },
    },
  };
  useEffect(() => {
    if (inView) {
      controls.start("show");
    } else {
      controls.start("hide");
    }
  }, [controls, inView]);
  const [selectedStep, setSelectedStep] = useState(1);
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [isToggled, setToggled] = useState(false);
  const [selectedItem, setSelectedItem] = useState(2);
  
  // const handleToggle = (e) => {
  //   e.preventDefault();
  //   setToggled(!isToggled);
  // };
  const handleItemClick = (item) => {
    setSelectedItem(item);
  };
  const handleDateSelect = (date) => {
    setSelectedDate(date);
    setToggled(!isToggled);
  };
  const updateSelectedItem = () => {
    const nextItem = selectedItem < 5 ? selectedItem + 1 : 1;

    if (nextItem === 5) {
      setToggled(true);
    } else {
      setSelectedItem(nextItem); // Ensure isToggled is set to false for other steps
    }
  };
  const handleStepChange = (step) => {
    setSelectedStep(step);
    const newDate = new Date(selectedDate);
    const daysToAdd = 7 * (step - 1);
    newDate.setDate(newDate.getDate() + daysToAdd); // Increment by 7 days for each step
    const lastDayOfMonth = new Date(
      newDate.getFullYear(),
      newDate.getMonth() + 1,
      0
    ).getDate();
    if (newDate.getDate() > lastDayOfMonth) {
      newDate.setDate(lastDayOfMonth);
    }
    if (newDate.getMonth() !== selectedDate.getMonth()) {
      newDate.setDate(1);
    }
    setSelectedDate(newDate);
  };
  useEffect(() => {
    const intervalId = setInterval(() => {
      updateSelectedItem();
    }, 700);

    return () => {
      clearInterval(intervalId);
      setToggled(false);
    };
  }, [selectedItem, controls]);

  const itemVariants = {
    hide: { opacity: 0, x: -20 },
    show: { opacity: 1, x: 0 },
  };
  const listVariants = {
    show: {
      transition: {
        staggerChildren: 0.2, // Adjust the stagger duration as needed
      },
    },
  };
  const isToday = (date, item) => {
    const today = new Date();
    const targetDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 7 * (item - 1));
    return (
      today.getFullYear() === targetDate.getFullYear() &&
      today.getMonth() === targetDate.getMonth() &&
      today.getDate() === targetDate.getDate()
    );
  }; 
  return (
    <motion.div
      className="wrf-right"
      style={{ paddingTop: '10px'}}
      initial="hide"
      animate="show"
      variants={introPictureVariants}
    >
      <lottie-player
          id="firstLottie"
          ref={ref}
          autoplay
          background="transparent"
          speed="0.8"
         // loop
          mode="normal"
          src="lottie/Widget_2_desktop.json"
          style={{ width: "400px",
                   height: "480px", 
                   border: "1px solid #EAEAED",
                   borderRadius: "20px",
                   boxShadow: "0px 23.852px 83.483px -19.082px rgba(0, 0, 0, 0.12)", 
                }}
        ></lottie-player>
    </motion.div>
  );
};

export default Step2;
