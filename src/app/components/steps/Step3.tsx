import React, { useState, useEffect,useRef } from "react";
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
  
const Step3 = () => {
  const controls = useAnimation();
  const reff = useRef(null);
  React.useEffect(() => {
    import("@lottiefiles/lottie-player");
  });
  const [selectedItem, setSelectedItem] = useState(1);
  const updateSelectedItem = () => {
    const nextItem = selectedItem < 4 ? selectedItem + 1 : 1;
    setSelectedItem(nextItem);
  };
  const [ref, inView] = useInView({
    triggerOnce: true, // Only trigger once when it comes into view
  });
  const introPictureVariants = {
    hide: {
      opacity: 0,
      scale: 0.5, // Change to a positive value for "down to up"
    },
    show: {
      opacity: 1,
      scale: 1, // Final scale when shown
      transition: {
        duration: 0.5,
      },
    },
  };
  useEffect(() => {
    if (inView) {
      controls.start("show");
    } else {
      controls.start("hide");
    }
  }, [controls, inView]);
  useEffect(() => {
    controls.start("show");
    const intervalId = setInterval(() => {
      updateSelectedItem();
    }, 1500);
    return () => {
      clearInterval(intervalId);
    };
  }, [selectedItem, controls]);
  const itemVariants = {
    hide: { opacity: 0, x: -20 },
    show: { opacity: 1, x: 0 },
  };
  const listVariants = {
    show: {
      transition: {
        staggerChildren: 0.2, // Adjust the stagger duration as needed
      },
    },
  };
  return (
    <motion.div
      className="wrf-right"
      style={{ paddingTop: '10px'}}
      initial="hide"
      animate="show"
      variants={introPictureVariants}
    >   
      <lottie-player
          id="firstLottie"
          ref={ref}
          autoplay
         // loop
          background="transparent"
          speed="0.7"
          mode="normal"
          src="lottie/Widget_3_desktop.json"
          style={{ width: "400px",
                   height: "480px", 
                   border: "1px solid #EAEAED",
                   borderRadius: "20px",
                   boxShadow: "0px 23.852px 83.483px -19.082px rgba(0, 0, 0, 0.12)", 
                }}
        ></lottie-player>
    </motion.div>
  );
};

export default Step3;
