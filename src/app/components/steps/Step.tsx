import React, { useState, useEffect } from "react";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
const Step = () => {
  const controls = useAnimation();
  const [ref, inView ,entry] = useInView({
    triggerOnce: true, 
    threshold: 0.75,
  });
  const [currentStep, setCurrentStep] = useState(0);
  const [intervalId, setIntervalId] = useState(null);
  const [stepKey, setStepKey] = useState(0);
  const [resetWidth, setResetWidth] = useState(false);
 
  const handleAnimationComplete = () => {
    
    //setCurrentStep((currentStep) => (currentStep % 3) + 1); 
    setCurrentStep((currentStep) => {
      const nextStep = currentStep + 1;
      if (nextStep > 3) {
          // Add a delay before resetting to step 1
          setTimeout(() => {
              setCurrentStep(1);
          }, 3000); // 3 seconds delay
      } else {
          return nextStep;
      }
  });
  };
 

  const renderStep = () => {
    switch (currentStep) {
      case 1:
        return <Step1 key={stepKey} onRadioClick={handleRadioClick} />;
      case 2:
        return <Step2 key={stepKey} onRadioClick2={handleRadioClick2} />;
      case 3:
        return <Step3 key={stepKey} />;
      default:
        return null;
    }
  };
  const handleRadioClick2 = () => {
    setCurrentStep(3);
  };
  const handleRadioClick = () => {
    setCurrentStep(2);
  };

  const handleStep = (step) => {
    setResetWidth(true);
    setTimeout(() => {
      setResetWidth(false);
    }, 100);
    const intervalDuration = 7000;
    const activeLi = document.querySelector(
      ".how-it-work .wrk-left ul li.active"
    );
    if (activeLi) {
      activeLi.classList.add("reset");
    }
    const animationContainer = document.querySelector(".wrf-right");
    if (animationContainer) {
      animationContainer.classList.add("reset");
    }
    setTimeout(() => {
      if (activeLi) {
        activeLi.classList.remove("reset");
      }
      if (animationContainer) {
        animationContainer.classList.remove("reset");
      }
    }, 100);
    setCurrentStep(step);
    setStepKey((prevKey) => prevKey + 1);
    clearInterval(intervalId);
    const newIntervalId = setInterval(() => {
      handleAnimationComplete();
    }, intervalDuration);

    setIntervalId(newIntervalId);
    
  };
  const introPictureVariants: Variants = {
    hide: {
      opacity: 0,
      y: 50,
    },
    show: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        ease: "easeInOut",
      },
    },
  };

  useEffect(() => {
    if (inView) {
      controls.start("show");
    }
    const animationDuration = 1.6;
    const intervalDuration = 7000;
    const id = setInterval(() => {
      if (inView) {
        handleAnimationComplete();
      } 
    }, intervalDuration);
    setIntervalId(id);
    return () => clearInterval(id);
  }, [controls, inView]);

  return (
    <motion.div
     // initial="hide"
      animate={controls}
      exit="hide"
      variants={introPictureVariants}
      transition={{ duration: 0.6, ease: "easeInOut" }}
      onAnimationComplete={handleAnimationComplete}
    >
      <div className="how-it-work">
        <div className="container">
          <div className="how-work-title">
            <h2>
              How it <span>works</span>
            </h2>
          </div>
          <div className="wek-main">
            <div className="wrk-left">
              <ul>
                <li 
                  className={` ${currentStep === 1 ? "active" : ""} ${
                    resetWidth ? "reset" : ""
                  }`}
                  onClick={() => {
                    handleStep(1);
                  }}
                >
                  <h6 ref={ref} >Step 1</h6>
                  <h4>Select Livble as your payment method</h4>
                  <p>Available in your tenant portal</p>
                </li>
                <li 
                  className={` ${currentStep === 2 ? "active" : ""} ${
                    resetWidth ? "reset" : ""
                  }`}
                  onClick={() => {
                    handleStep(2);
                  }}
                >
                  <h6  >Step 2</h6>
                  <h4  >Choose the plan that best suits you</h4>
                  <p  >Select the preferred dates</p>
                </li>
                <li 
                  className={` ${currentStep === 3 ? "active" : ""} ${
                    resetWidth ? "reset" : ""
                  }`}
                  onClick={() => {
                    handleStep(3);
                  }}
                >
                  <h6 >Step 3</h6>
                  <h4>Split your rent - we take care of the rest!</h4>
                  <p>
                  You're all set for the month!
                  </p>
                </li>
              </ul>
            </div>
            {renderStep()}
          </div>
          <div className="step-ref"></div>
        </div>
      </div>
    </motion.div>
  );
};

export default Step;
