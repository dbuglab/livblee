// ThreeJSBookPage.js
import React, { useRef, useState } from 'react';
import { Canvas, useThree, useFrame } from '@react-three/fiber';

const BookPage = () => {
  const mesh = useRef();
  const [scrollY, setScrollY] = useState(0);

  const handleScroll = () => {
    setScrollY(window.scrollY);
  };

  useFrame(() => {
    const rotation = Math.min(90, scrollY / 3);
    mesh.current.rotation.x = degreesToRadians(rotation);
  });

  return (
    <mesh ref={mesh}>
      {/* Add your book page geometry/material here */}
      <planeBufferGeometry args={[1, 1, 1]} />
      <meshStandardMaterial color={'#ffffff'} />
    </mesh>
  );
};

const ThreeJSBookPage = () => {
  return (
    <Canvas>
      <ambientLight />
      <pointLight position={[10, 10, 10]} />
      <BookPage />
    </Canvas>
  );
};

export default ThreeJSBookPage;
