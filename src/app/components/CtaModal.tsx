import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faTimes } from "@fortawesome/free-solid-svg-icons";
import { motion } from "framer-motion";

const CtaModal = ({ isOpen, onClose }) => {
  if (!isOpen) {
    return null;
  }
  return (
    <>
      <motion.div
        className="modal"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >
        <div className="modal-content">
          <button className="modal-btn" onClick={onClose} style={{ color: '#7E8288' }}>          
            <FontAwesomeIcon icon={faTimes} />
          </button>
          <h2>Find Livble in your tenant portal</h2>
          <div className="item">
            
          <img src="images/lock-closed.png" />
            <h3>Sign in to your tenant portal</h3>
          </div>
          <img src="svg/Vector 2185.svg"   className="arrow1" />
          <div  className="item">
          <img src="images/banknotes.png" />
            <h3>Go to the payment section</h3>
          </div>
          <img src="svg/Vector 2185.svg"  className="arrow1" />
          <div  className="item">
          <img src="images/Frame 7264.png" />
            <h3>Find Livble as a payment option</h3>
          </div>
          <img src="svg/Vector 2185.svg"  className="arrow1" />
          <div  className="item">
          <img src="images/frame321.png" />
            <h3>Select Livble (and the rest is history!)</h3>
          </div>
       <a href="#" onClick={onClose} >Done</a>
        </div>
      </motion.div>
    </>
  );
};

export default CtaModal;
