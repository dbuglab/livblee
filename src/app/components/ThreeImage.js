import React, { useRef } from 'react';
import { Canvas } from 'react-three-fiber';
import { TextureLoader } from 'three';
import { useFrame } from 'react-three-fiber';

const ThreeDImage = ({ url, scrollProgress }) => {
  const mesh = useRef();
  const texture = new TextureLoader().load('images/Frame%207332%20(1).png');
  // Use the scroll progress to update the position and scale of the image
  useFrame(() => {
    if (mesh.current) {
      mesh.current.position.y = scrollProgress * 2; // Adjust the factor based on your preference
      mesh.current.scale.set(1 + scrollProgress * 2, 1 + scrollProgress * 2, 1);
    }
  });

  return (
    <mesh ref={mesh}>
      <boxGeometry  attach="geometry" args={[1, 1]} />
      <meshBasicMaterial attach="material" transparent>
        <texture url={url} />
      </meshBasicMaterial>
    </mesh>
  );
};

export default ThreeDImage;
