import React, { useState , useEffect } from 'react';
import SliderComponent from "./SliderComponent";

import Step from "./steps/Step";
import StepMob from "./steps/StepMob";
import { useForm } from 'react-hook-form';
import { motion , useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
import CtaModal from './CtaModal';

const Body = ({openModal}) => {
 
  
  const controls = useAnimation();
  const [ref, inView] = useInView({
    triggerOnce: true, // Only trigger once when it comes into view
  });
  const {
    handleSubmit,
    register,
    control,
    reset,
    getValues,
    formState: { errors },
  } =  useForm();

  const onSubmit = (data) => {
    console.log(data);
    // Handle form submission logic here
    reset();
  };
  const portal: Variants = {
    hide: {
      opacity: 0,
      y: 50, // Move up slightly
    },
    show: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 2,
        ease: 'easeInOut',
      },
    },
  };
 

  const introPictureVariants: Variants = {
    hide: {
        opacity: 0,
        x: 0,
    },
    show: {
        opacity: 1,
        y: -20,
      
        transition: {
            duration: 1,
        },
    },
};
const banner: Variants = {
  hide: {
    opacity: 0,
    y: 50, // Change to a positive value for "down to up"
  },
  show: {
    opacity: 1,
    y: 0,
  
    transition: {
      duration: 2,
    },
  },
};
const imageVariants = {
  hide: {
    opacity: 0,
    scale: 0.5,// Change to a positive value for "down to up"
  },
  show: {
    opacity: 1,
    scale: 1, // Final scale when shown
    transition: {
      duration: 0.5,
    },
  },
};
const img1Variants = {
  hide: {
    opacity: 0,
    scale: 0.5,// Change to a positive value for "down to up"
  },
  show: {
    opacity: 1,
    scale: 1, // Final scale when shown
  },
};
const footer: Variants = {
  hide: {
    opacity: 0,
    y: 50, // Move up slightly
  },
  show: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.5,
      ease: 'easeInOut',
    },
  },
};
useEffect(() => {
  if (inView) {
    controls.start('show');
  } else {
    controls.start('hide');
  }
}, [controls, inView]);

  return (
    <>
   
     <motion.div
      initial="hide"
      animate="show"
      exit="hide"
      transition = {{duration: 0.6, ease: 'easeInOut' } }
      variants={banner}
    >
      <div className="banner-sec">
        <div className="container">
          <div className="main-bnr">
            <div className="bnr-cnt">
              <h1>
              Split <span>your rent.</span>
              </h1>
              <p>
              Pay your rent in up to 4 installments <br />
              over the month.
              </p>
              <a href="#"  className="pay-btn" onClick={openModal}>
               Split My Rent
              </a>
              <a href="/partners" className="manage-btn desktop--sec">
                Livble For Partners 
                <img src="images/Frame.png" />
              </a>
            </div>
            
            <motion.div
              className="bnr-img"
              initial="hide"
              animate="show"
              variants={imageVariants}
            >
              <img src="images/dummy_portal_svg.svg" />
              <motion.img
                src="images/Group%206851.png"
                className="img1"
                initial="hide"
                animate="show"
                variants={img1Variants}
                transition={{ delay: 0.8 }}
              />    
              <motion.img
                src="images/Group%206871.png"
                className="img2"
                initial="hide"
                animate="show"
                variants={img1Variants}
                transition={{ delay: 1 }}
                
              />   
               <motion.img
                src="images/Mask%20group.png"
                className="img3"
                initial="hide"
                animate="show"
                variants={img1Variants}
                transition={{ delay: 0.5 }}
              />     
             
              {/* <img src="images/Mask%20group.png" className="img3" /> */}
             
              </motion.div>
          </div>
        </div>
      </div>
      <div className='main-bnr  pt0'>
        <div className='bnr-cnt mbl-sec'>
        <a href="#" className="manage-btn">
                Livble For Partners 
                <img src="images/Frame.png" />
              </a>
        </div>
      </div>
      </motion.div>
      <div className="icon-sec-div">
        <div className="icon-main">
          <img src="images/Frame%207382.png" />
          <p>Avoid late fees &amp; credit card fees. We've got your back!</p>
        </div>
        <div className="icon-main">
          <img src="images/Layer_2.png" />
          <p>Link rent to payroll. Pay when you get paid!</p>
        </div>
        <div className="icon-main">
          <img src="images/Group%206803.png" />
          <p>Build your credit through rent. Improve your financial future</p>
        </div>
        <div className="icon-main">
          <img src="images/Group%206802.png" />
          <p>Manage your expenses. Get back on track!</p>
        </div>
      </div>
     <div className='demo'><Step /></div> 
     <div className='demo-mob'><StepMob /></div> 
      <motion.div
      className="app-sec"
       initial="hide"
       whileInView="show"
       exit="hide"
      animate={{ opacity: 0, y: 10 }}
      transition = {{duration: 0.6, ease: 'easeInOut' } }
      variants={introPictureVariants}
    >
     
        <div className="container">
          <div className="app-main-sec">
            <div className="app-left-sec">
              <img src="images/Group6851.png" />
              <h4>
                Pro tip: Download the Livble app{" "}
                <span>Get access to the newest perks!</span>
              </h4>
            </div>
            <div className="app-right-sec">
              <a href="#">
                <img src="images/Vector (3).png" />
                App Store
              </a>
              <a href="#">
                <img src="images/Icon.png" />
                Google Play
              </a>
            </div>
          </div>
        </div>
     
      </motion.div>
      <div className="logo-sec">
        <div className="logo-title">
          <h2>Trusted by</h2>
        </div>
        <SliderComponent />
      </div>
      {/* <motion.div
    ref={ref}
      initial="hide"
      animate={controls}
      exit="hide"
      variants={portal}
      transition={{ duration: 2, ease: 'easeInOut' }}
  > */}
      <div className="frm-detail">
        <div className="frm-left">
          <h3>Don’t see your portal here?</h3>
          <p>
            Let us know if you want this payment option in your portal, and
            we’ll notify you when it’s available
          </p>
          <form>
          {/* // onSubmit={handleSubmit(onSubmit)} */}
         
            <input type="text" placeholder="Your name" 
            // {...register('name', { required: 'Full name is required' })} 
            />
            {/* {errors.name && <span style={{ color: 'black' }}>{errors.name.message}</span>} */}
            <input type="text" placeholder="Email address"
            //  {...register('email', { required: 'email is required' })}
             />
            {/* {errors.email && <span style={{ color: 'black' }}>{errors.email.message}</span>} */}
            <input type="text" placeholder="Portal name" 
            // {...register('portal', { required: 'portal is required' })} 
            />
           
            <button type="button">Send</button>
          </form>
        </div>
        <div className="frm-right-img">
          <img src="images/image 10.svg" />
        </div>
      </div>
      {/* </motion.div> */}
     
      
        <div className="ftr-upr-main-title">
          <div className="ftr-title">
            <h2>
              <span>Empowering </span>the next generation of tenants to rent
              with <span>confidence</span>
            </h2>
          </div>
        </div>
      
    </>
  );
};

export default Body;
