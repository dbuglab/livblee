import React, { useState , useEffect } from 'react';
import { motion , useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
const Footer = () => {
  const controls = useAnimation();
  const [ref, inView] = useInView({
    triggerOnce: true, // Only trigger once when it comes into view
  });
  const footer: Variants = {
    hide: {
      opacity: 0,
      y: 50, // Move up slightly
    },
    show: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 2,
        ease: 'easeInOut',
      },
    },
  };
  useEffect(() => {
    if (inView) {
      controls.start('show');
    } else {
      controls.start('hide');
    }
  }, [controls, inView]);
  return (
    <motion.div
    // ref={ref}
    //   initial="hide"
    //   animate={controls}
    //   exit="hide"
    //   variants={footer}
    //   transition={{ delay: 1, ease: 'easeInOut'  }}
      
  >
    <footer>
      <div className="container">
        <div className="ftr-app">
          <div className="ftr-logo">
            <img src="images/Group f.svg" />
            <p>contact@nowlvble.com</p>
          </div>
          <div className="ftr-app-btn">
            <a href="#">
              <img src="images/Vector (3).png" />
              App Store
            </a>
            <a href="#">
              <img src="images/Icon.png" />
              Google
            </a>
          </div>
        </div>
        <div className="ftr-btm">
          <div className="ftr-link">
            <ul>
              <li>
                <a href="#">Privacy policy</a>
              </li>
              <li>
                <a href="#">Terms &amp; conditions</a>
              </li>
            </ul>
          </div>
          <div className="copy-right">
            <p>Private and confidential © Copyright Livble</p>
          </div>
        </div>
      </div>
    </footer>
    </motion.div>
  );
};

export default Footer;
