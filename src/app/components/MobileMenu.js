// MobileMenu.js
import React from 'react';
import Link from "next/link";
import { usePathname } from 'next/navigation'
const MobileMenu = ({ isOpen, onClose }) => {
    const pathname = usePathname()
  return (
    <div className={`mobile-menu ${isOpen ? 'open' : ''}`}>
     <div className="menu-bar">
              <ul>
                <li className={pathname === '/' ? 'active' : ''}>
                  <Link href="/">Tenants</Link>
                </li>
                <li className={pathname === '/property' ? 'active' : ''}>
                  <Link href="/property">Partners</Link>
                </li>
                <li className={pathname === '/about' ? 'active' : ''}>
                  <Link href="/about">About Page</Link>
                </li>
              </ul>
              </div>
      {/* <button onClick={onClose}>Close</button> */}
    </div>
  );
};

export default MobileMenu;
