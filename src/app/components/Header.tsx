import Link from "next/link";
import Head from "next/head";
import React, { useEffect ,useState ,useRef   } from 'react';
import { usePathname } from 'next/navigation'
import { motion } from 'framer-motion';
import MobileMenu from '../components/MobileMenu';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars ,faTimes  } from '@fortawesome/free-solid-svg-icons';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';

const Header = React.forwardRef(({ currentPage ,headerBackgroundRef }, ref) => {
  const [isMobileMenuOpen, setMobileMenuOpen] = useState(false);
  const [isScrolled, setIsScrolled] = useState(false);
  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;
      setIsScrolled(scrollPosition > 0);
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
  const handleMobileMenuToggle = () => {
    //setMobileMenuOpen(!isMobileMenuOpen);
    handleToggle();
   
  };
  const handleMobileMenuClose = () => {
    setMobileMenuOpen(false);
  };
  const [isToggled, setToggled] = useState(true);
  const pathname = usePathname()
  const isHomePage = currentPage === '/';
  const handleToggle = () => {
    setToggled(prevState => !prevState);
  };
  const introPictureVariants: Variants = {
    hide: {
        opacity: 0,
        x: -20,
    },
    show: {
        opacity: 1,
        y: -20,
      
        transition: {
            duration: 2,
        },
    },
};
const [currentBackgroundColor, setCurrentBackgroundColor] = useState(null);

useEffect(() => {
    const headerElement = document.querySelector(".home-header");

    if (headerElement) {
      const computedStyle = window.getComputedStyle(headerElement);
      const backgroundColor = computedStyle.backgroundColor.toLowerCase();
      setCurrentBackgroundColor(backgroundColor);    
    }

  }, []);

const isPropertyPage = pathname === '/partners';
console.log("home-header",currentBackgroundColor)
  return (
    <>
 
      <Head>
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@100;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet"
        />
        <link href="https://cdn.rawgit.com/kenwheeler/slick/master/slick/slick.css" />
        <link href="https://cdn.rawgit.com/kenwheeler/slick/master/slick/slick-theme.css" />
      </Head>

      <header ref={ref}  className={` ${isScrolled ? 'scrolled' : ''} ${isHomePage ? 'home-header ' : 'other-header '}`}>
      <motion.div
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      exit={{ opacity: 0, y: -50 }}
      transition = {{duration: 0.6, ease: 'easeInOut' } }
      variants={introPictureVariants}
    >
        <div className={`container ${isToggled ? 'true' : 'false'}`}>
          <div className="hdr-inr">
            <div className="logo">
            <Link href="/">
              <img src="images/Group 9.svg" />
            </Link>
            </div>
            {/* <button onClick={handleToggle}>
      {isToggled ? 'Toggle Off' : 'Toggle On'}
    </button> */}
      <button className="menu-desk" id="menu-desk" onClick={handleMobileMenuToggle}>
      {currentBackgroundColor === 'rgb(11, 28, 25)' ? (
        <img src="images/Menuw.png" alt="Menu" />
      ) : (
        isPropertyPage ? (
          isScrolled ? (
            <img src="svg/Menu.svg" alt="Menu" />
          ) : (
            <img src="images/Menuw.png" alt="Menu" />
          )
        ) : (
          <img  src="svg/Menu.svg" alt="Menu" />
        )
      )}
        </button>
      <button  className="menu-mobl" onClick={handleMobileMenuToggle}> <FontAwesomeIcon icon={faTimes} /> </button>
      {/* <MobileMenu isOpen={isMobileMenuOpen} onClose={handleMobileMenuClose} /> */}
            <div className="menu-bar">
              <ul >
                <li className={` ${pathname === '/' ? 'about' : ''} ${pathname === '/' ? 'active' : ''} `} >
                  <Link onClick={handleToggle} href="/">Tenants</Link>
                </li>
                <li className={` ${pathname === '/' ? 'about' : ''} ${pathname === '/partners' ? 'active' : ''} `}>
                  <Link onClick={handleToggle} href="/partners">Partners</Link>
                </li>
                <li className={` ${pathname === '/' ? 'about' : ''} ${pathname === '/about' ? 'active' : ''}`}>
                  <Link onClick={handleToggle} href="/about">About</Link>
                </li>
              </ul>
               <div className="ftr-app">
                <h5>Get in touch</h5>
          <div className="ftr-logo">
           
            <p>contact@nowlvble.com</p>
          </div>
          <div className="ftr-app-btn" >
            <a  href="#">
              <img src="images/Vector (3).png" />
              App Store
            </a>
            <a  href="#">
              <img src="images/Icon.png" />
              Google
            </a>
          </div>
        </div>
            </div>
            <div className="icon-sec"> 
            {/* style={{color: isScrolled ? ' ' : 'white !important '}}  */}
            {isPropertyPage ? 
                  <a className={` ${pathname === '/partners' ? '' : ''} `} href="#">
                    Contact Us
                  </a>
                :  <ul>
                <li>
                  <a className={`${pathname === '/' ? 'about' : ''}`} href="#">
                    <img src="images/Icon-blck2.png" />
                    App Store
                  </a>
                </li>
                <li>
                  <a className={`${pathname === '/' ? 'about' : ''}`} href="#">
                    <img src="images/Icon-blck1.png" />
                    Google Play
                  </a>
                </li>
              </ul>}
             
            </div>
          </div>
        </div>
        </motion.div>
      </header>
    </>
  );
});

export default Header;
