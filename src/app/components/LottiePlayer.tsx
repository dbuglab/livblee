import React, { useEffect, useRef } from 'react';

const LottiePlayer = ({ stepNumber, animationSrc }) => {
  const ref = useRef(null);

  useEffect(() => {
    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            // Start the animation when the step is in view
            ref.current.play();
          } else {
            // Pause the animation when the step is not in view
            ref.current.pause();
          }
        });
      },
      { threshold: 0.5 } // Adjust the threshold as needed
    );

    observer.observe(ref.current);

    return () => {
      observer.unobserve(ref.current);
    };
  }, []);

  return (
    <lottie-player
      ref={ref}
      autoplay={false} // Set autoplay to false since we're controlling it with Intersection Observer
      background="transparent"
      speed="1"
      loop
      mode="normal"
      src={animationSrc}
      style={{ width: "353px", height: "475px" }}
    ></lottie-player>
  );
};

export default LottiePlayer;
"use client";
import { useEffect, useRef } from "react";
import Header from "../components/Header";
import { gsap, TimelineLite } from "gsap";
import { Link as ScrollLink } from "react-scroll";
import Footer from "../components/Footer";
import { motion, useAnimation } from "framer-motion";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { usePathname } from "next/navigation";
import SmoothScrolling from "../components/SmoothScrolling";

gsap.registerPlugin(ScrollTrigger);

const About = () => {
  const missionRef = useRef(null);
  const missionHeadRef = useRef(null);
  const visionRef = useRef(null);
  const visionHeadRef = useRef(null);
  const valuesRef = useRef(null);
  const valuesHeadRef = useRef(null);
  const abtRef = useRef(null);
  const trustRef = useRef(null);
  const backedRef = useRef(null);
  const employeeRef = useRef(null);

  const headerRef = useRef(null);
  const aboutRef = useRef(null);

  const isMobile = window.innerWidth < 768;

  const missionHeadReff = useRef(null);
  const visionHeadReff = useRef(null);
  const ourHeadRef = useRef(null);
  console.log("window", window.innerWidth);
  useEffect(() => {
    // Adjust the mobile breakpoint as needed
    const isSmallLaptop = window.innerWidth >= 550 && window.innerWidth < 1024; // Adjust the small laptop breakpoint as needed
    const isDesktopWide =
      window.innerWidth >= 1024 && window.innerWidth >= 1250;
    console.log("isMobile", isMobile);
    console.log("isSmallLaptop", isSmallLaptop);
    console.log("isDesktopWide", isDesktopWide);
    const missionElement = missionRef.current;
    const visionElement = visionRef.current;
    const valueElement = valuesRef.current;
    const abtElement = abtRef.current;

    const trustElement = trustRef.current;
    const backedElement = backedRef.current;
    const employeeElement = employeeRef.current;

    const links = abtElement.querySelectorAll(".abt-logobtn a");

    // const liItems = valueElement.querySelectorAll('.abt-icn ul li');
    const animateLinks = () => {
      links.forEach((link, index) => {
        gsap.fromTo(
          link,
          {
            y: isMobile ? 0 : 50, // Initial position (adjust as needed)
            opacity: 0,
            scale: 0.5,
          },
          {
            y: 0,
            opacity: 1,
            scale: 1,
            duration: isMobile ? 0.7 : 1,
            ease: "power4.easeInOut",
            delay: index * 0.5, // 1-second delay for each link
          }
        );
      });
    };

    console.log("first", window.innerWidth);
    const animateElement = (element) => {
      gsap.from(element, {
        y: isMobile ? 0 : 0,
        opacity: 0,
        duration: isMobile ? 0.7 : 0.7,
        //delay:2,
        ease: "bounce.out",
        scrollTrigger: {
          trigger: element,
          // start: isSmallLaptop ? "30% 98%" : "70% 94%",
          // end: "60% 20%",
          start: isMobile
            ? ""
            : window.innerWidth > 1400
            ? "70% 68%"
            : "70% 94%",
          end: isMobile ? "" : "60% 20%",
          //start: '45% 75%',
          //end: 'bottom 40%', 801  "70% 64%"
          //scrub: true,
            markers: true,
          offset: -100,
          stagger: 0.5,
          onEnter: () => {
            gsap.fromTo(
              element,
              {
                y: isMobile ? window.innerHeight : window.innerHeight,
                opacity: 0,
              },
              {
                y: 0,
                opacity: 1,
                duration: isMobile ? 0.7 : 1,
                ease: "power4.out",
              }
            );

            gsap.fromTo(
              missionHeadReff.current,
              {
                y: isMobile ? 0 : 0,
                scaleY: isMobile ? 0 : 8,
                scaleX: isMobile ? 0 : 8,
                scale: isMobile ? 0 : 8,
                opacity: isMobile ? 1 : 0,
                duration: isMobile ? 0 : 1,
                ease: isMobile ? " " : "power4.out",
              },
              {
                y: isMobile ? 0 : 0,
                scaleY: isMobile ? 0 : 1,
                scaleX: isMobile ? 0 : 1,
                scale: isMobile ? 0 : 1,
                opacity: isMobile ? 1 : 1,
                duration: isMobile ? 0.7 : 1,
                ease: "power4.out",
              }
            );
            gsap.fromTo(
              visionHeadReff.current,
              {
                y: isMobile ? 0 : 0,
                scaleY: isMobile ? 0 : 8,
                scaleX: isMobile ? 0 : 8,
                scale: isMobile ? 0 : 8,
                opacity: isMobile ? 0 : 0,
                duration: isMobile ? 0 : 1,
                ease: isMobile ? " " : "power4.out",
              },
              {
                y: isMobile ? 0 : 0,
                scaleY: isMobile ? 0 : 1,
                scaleX: isMobile ? 0 : 1,
                scale: isMobile ? 0 : 1,
                opacity: isMobile ? 0 : 1,
                duration: isMobile ? 0.7 : 1,
                ease: "power4.out",
              }
            );
          },
          onLeave: () => {
            // Check if the scroll direction is down and progress is within the specified range

            gsap.to(element, {
              y: 0,
              opacity: 1,
              duration: isMobile ? 0.7 : 1,
              ease: "power4.out",
            });
          },
          onLeaveBack: () => {
            // Reset opacity to 0 when scrolling back to the top
            gsap.to(element, {
              opacity: 0,
              duration: isMobile ? 0.7 : 1,
              ease: "power4.out",
            });
          },
        },
      });
    };

    animateElement(missionElement);
    animateElement(visionElement);
    gsap.from(valuesRef.current, {
      y: isMobile ? 0 : 0,
      opacity: isMobile ? 1 : 0,
      duration: isMobile ? 0.7 : 0.7,
      ease: "power4.out",
      scrollTrigger: {
        trigger: valuesRef.current,
        // start: isSmallLaptop ? "30% 98%" : "70% 94%",
        // end: "60% 20%",
        start: isMobile ? "" : isSmallLaptop ? "30% 98%" : "70% 94%",
        end: isMobile ? "" : "60% 20%",
        // markers: true,
        //stagger: 0.5,
        offset: -100,
        onEnter: () => {
          gsap.fromTo(
            valuesRef.current,
            {
              y: isMobile ? 100 : window.innerHeight,
              opacity: isMobile ? 1 : 0,
            },
            {
              y: 0,
              opacity: 1,
              duration: isMobile ? 0.7 : 1,
              ease: "power4.out",
            }
          );
        },
        onLeave: () => {
          gsap.to(valuesRef.current, {
            y: 0,
            opacity: 1,
            duration: isMobile ? 0.7 : 1,
            ease: "power4.out",
          });
        },
        onLeaveBack: () => {
          gsap.to(valuesRef.current, {
            opacity: isMobile ? 1 : 0,
            duration: isMobile ? 0.7 : 1,
            ease: "power4.out",
          });
        },
      },
    });
    gsap.from(abtRef.current, {
      y: isMobile ? 0 : 0,
      opacity: 0,
      duration: isMobile ? 0.7 : 0.7,
      //delay:2,
      ease: "bounce.out",
      scrollTrigger: {
        trigger: abtRef.current,
        // start: isSmallLaptop ? "30% 98%" : "70% 94%",
        // end: "60% 20%",
        start: isMobile ? "" : isSmallLaptop ? "30% 98%" : "70% 94%",
        end: isMobile ? "" : "60% 20%",
        //start: '45% 75%',
        //end: 'bottom 40%',
        //scrub: true,
        // markers: true,
        stagger: 0.5,
        offset: -100,
        onEnter: () => {
          gsap.fromTo(
            abtRef.current,
            {
              y: isMobile ? 50 : window.innerHeight,
              opacity: 0,
            },
            {
              y: 0,
              opacity: 1,
              duration: isMobile ? 0.7 : 1,
              ease: "power4.out",
            }
          );
          animateLinks();
          // animateValues();
        },
        onLeave: () => {
          // Check if the scroll direction is down and progress is within the specified range
          gsap.to(abtRef.current, {
            y: 0,
            opacity: 1,
            duration: isMobile ? 0.7 : 1,
            ease: "power4.out",
          });
        },
        onLeaveBack: () => {
          // Reset opacity to 0 when scrolling back to the top
          gsap.to(abtRef.current, {
            opacity: 0,
            duration: isMobile ? 0.7 : 1,
            ease: "power4.out",
          });
        },
      },
    });
    animateElement(trustElement);
    animateElement(backedElement);
    animateElement(employeeElement);
  }, []);
  const textRef = useRef(null);
  const introPictureVariants = {
    hide: {
      opacity: 0,
      scale: 1, // Change to a positive value for "down to up"
    },
    show: {
      opacity: 1,
      scale: 1, // Final scale when shown
      transition: {
        duration: 0.5,
      },
    },
  };
  const arrowRef = useRef(null);

  useEffect(() => {
    const tl = gsap.timeline({ repeat: -1, yoyo: true });

    tl.to(arrowRef.current, {
      y: 20,
      duration: 0.5,
      ease: "power1.inOut",
    });
  }, []);

  useEffect(() => {
    const chars = Array.from(textRef.current.textContent);

    const tl = new TimelineLite();

    chars.forEach((char, index) => {
      tl.fromTo(
        textRef.current.children[index],
        { opacity: 0, x: -20 },
        {
          opacity: 1,
          x: 0,
          duration: 0.2,
          ease: "power2.inOut",
        },
        index * 0.1
      );
    });

    gsap.to(tl, {
      duration: 0.1 * chars.length,
      onComplete: () => {
        gsap.set(textRef.current.children, { clearProps: "all" });
      },
    });
  }, []);

  return (
    <>
      <Header currentPage="/" ref={headerRef} />
      <SmoothScrolling>
        <div className="abut-sec">
          <div className="container">
            <div className="abt-main-div">
              <motion.img
                initial="hide"
                animate="show"
                variants={introPictureVariants}
                src="images/Group 9(1).svg"
              />
              <motion.h3
                initial="hide"
                animate="show"
                variants={introPictureVariants}
                ref={textRef}
              >
                {Array.from("is all about").map((char, index) => (
                  <span key={index}>{char}</span>
                ))}
              </motion.h3>
              <img
                ref={arrowRef}
                src="images/Vector%202182.png"
                className="arw"
              />

              {isMobile ? (
                <div className="animated-element">
                  {/* Your mobile-specific content */}
                  <ScrollLink to="vision" smooth={true} duration={500}>
                    <h5 className="mission-head">Our mission</h5>
                  </ScrollLink>
                  <p className="mission-text">
                    Helping Property Management Software platforms quickly and
                    seamlessly embed and offer financial solutions to their
                    clients, property managers, and tenants.
                  </p>
                </div>
              ) : (
                <div ref={missionRef} className="animated-element">
                  <ScrollLink to="vision" smooth={true} duration={500}>
                    {/* ref={missionHeadRef} */}

                    <h5 ref={missionHeadReff} className="mission-head">
                      Our mission
                    </h5>
                  </ScrollLink>

                  <p className="mission-text">
                    Helping Property Management Software platforms quickly and
                    seamlessly embed and offer financial solutions to their
                    clients, property managers, and tenants.
                  </p>
                </div>
              )}
              {isMobile ? (
                <div ref={visionRef} className="animated-element">
                  <ScrollLink to="nextSection" smooth={true} duration={500}>
                    {/* ref={visionHeadRef} */}

                    <h5 className="mission-head">Our vision</h5>
                  </ScrollLink>
                  <p>
                    Improving the lives of everyday people with financial
                    flexibility through affordable credit solutions.
                  </p>
                </div>
              ) : (
                <div ref={visionRef} className="animated-element">
                  <ScrollLink to="nextSection" smooth={true} duration={500}>
                    {/* ref={visionHeadRef} */}

                    <h5 ref={visionHeadReff} className="mission-head">
                      Our vision
                    </h5>
                  </ScrollLink>
                  <p>
                    Improving the lives of everyday people with financial
                    flexibility through affordable credit solutions.
                  </p>
                </div>
              )}
            </div>
          </div>
        </div>
        <div ref={valuesRef} className="valu-abt animated-element">
          <div className="container">
            <div className="abt-icn">
              <ScrollLink to="mission" smooth={true} duration={500}>
                {/* ref={valuesHeadRef} */}

                <h2 className="our-head">
                  Our <span>values</span>
                </h2>
              </ScrollLink>
              <ul>
                <li>
                  <img src="images/Frame%207424.png" />
                  <h5>Integrity</h5>
                </li>
                <li>
                  <img src="images/1122.png" />
                  <h5>Humility</h5>
                </li>
                <li>
                  <img src="images/111222.png" />
                  <h5>Dedication</h5>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div ref={aboutRef}></div>

        <div className="abt-logo-sec">
          <div className="container">
            <div ref={abtRef} className="abt-logo-title animated-element">
              <h3>
                Bringing embedded financing capabilities to <br /> Property
                Management Software
              </h3>
              <div className="abt-logobtn">
                <a hef="#">I’m a tenant</a>
                <a hef="#">I’m a partner</a>
              </div>
            </div>
            <div ref={trustRef} className="abt-inr-logo">
              <div className="abt-int-title">
                <h4>Trusted by</h4>
                <ul>
                  <li>
                    <img src="images/logo%203.png" />
                  </li>
                  <li>
                    <img src="images/Group.png" />
                  </li>
                  <li>
                    <img src="images/idAf0UDEwC%201.png" />
                  </li>
                  <li>
                    <img src="images/payrent_logo_inv%203.png" />
                  </li>
                </ul>
              </div>
            </div>
            <div ref={backedRef} className="abt-inr-logo">
              <div className="abt-int-title">
                <h4>Proudly backed by</h4>
                <ul>
                  <li>
                    <img src="images/Arc-Ventures-Full-Logo-SVG.png" />
                  </li>
                  <li>
                    <img src="images/image.png" />
                  </li>
                  <li>
                    <img src="images/Frame%207433.png" />
                  </li>
                  <li>
                    <img src="images/gemnZK.png" />
                  </li>
                  <li>
                    <img src="images/63051734859f6a2f8d19bacf_aloniq.png" />
                  </li>
                </ul>
              </div>
            </div>
            <div ref={employeeRef} className="abt-inr-logo">
              <div className="abt-int-title">
                <h4>With early employees from</h4>
                <ul>
                  <li>
                    <img src="images/logo-jpm-brown.png" />
                  </li>
                  <li>
                    <img src="images/Logo.png" />
                  </li>
                  <li>
                    <img src="images/image%20copy.png" />
                  </li>
                  <li>
                    <img src="images/image%20copy%203.png" />
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        </SmoothScrolling> 
      <Footer />
      
    </>
  );
};
export default About;


