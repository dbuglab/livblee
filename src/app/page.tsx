"use client";
import React, { useState, useEffect } from "react";
import Header from "./components/Header";
import Body from "./components/Body";
import Footer from "./components/Footer";
import CtaModal from './components/CtaModal';

const Home = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };
  useEffect(() => {
    if (isModalOpen) {
      document.body.classList.add('modal-open');
    } else {
      document.body.classList.remove('modal-open');
    }
    return () => {
      document.body.classList.remove('modal-open');
    };
  }, [isModalOpen]);
  return (
    <>
      <CtaModal isOpen={isModalOpen} onClose={closeModal} />
      <div className="main">
      <Header currentPage="/" />
      <Body openModal={openModal}/>
      <Footer />
      </div>
    </>
  );
};

export default Home;
